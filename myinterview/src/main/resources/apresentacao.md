Me chamo Diego, tenho 31 anos, sou formado em Sistemas de Informação, trabalhei por mais de dez anos como desenvolvedor onde adquiri afinidade e conhecimento em Banco de Dados. 

Desde muito jovem sou apaixonado por filosofia, estudo e pratico desde os 16 anos. Por isso, um dos meus hobbies é a literatura, tenho prazer em ler e escrever, pra mim é uma extensão da existência que complementa a experiência como ser humano.

Outro hobby é o motociclismo, pilotar é como meditar, consigo me conectar com meus pensamentos e sentimentos enquanto exercito a disciplina na pilotagem para me tornar um piloto cada vez melhor.

Ao longo da minha carreira, quase que acidentalmente, fui descobrindo mais sobre mim e sobre o que gosto de fazer, com isso aos poucos fui adquirindo conhecimento e experiência em áreas paralelas ao desenvolvimento como análise de negócios, requisitos, sucesso do cliente, agilidade e afins. Tenho muito interesse e gosto de trabalhar com metodologias ágeis nos vários papéis existentes.

Atualmente estou cursando formação em psicanálise e buscando adquirir conhecimento nas tecnologias mais recentes ligadas à banco de dados, o intuito é resgatar minha expertise técnica e atualizá-la ao mesmo tempo. Minhas características que mais se destacam são a paciência, a disciplina, o relacionamento interpessoal e a empatia.

Meu maior objetivo de vida consiste em me conhecer cada vez mais e impactar positivamente a vida de todos ao meu redor. Acredito que a tecnologia é um instrumento fabuloso para a melhoria na qualidade de vida das pessoas em geral e acredito que posso utilizá-la para tornar a história das pessoas um pouco mais bonita.