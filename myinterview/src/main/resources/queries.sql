#Query que retorna a quantidade de funcionários separados por sexo.
SELECT e.gender, 
       COUNT(e.gender) quantity 
  FROM employees e
GROUP BY e.gender;

#Query que retorna a quantidade de funcionários distintos por sexo, ano e ano de nascimento.
SELECT e.gender, 
       YEAR(e.hire_date) hire_year, 
       YEAR(e.birth_date) birth_year,  
       count(e.emp_no) quantity 
  FROM employees e 
GROUP BY e.gender, 
         YEAR(e.hire_date), 
         YEAR(e.birth_date);


#Query que retorna a média, min e max de salário por sexo.
SELECT e.gender,
       AVG(s.salary) average,
       MIN(s.salary) minimum,
       MAX(s.salary) maximum
  FROM salaries s, employees e
 WHERE s.emp_no = e.emp_no 
GROUP BY e.gender;