package com.example;

/**
 * 
 *
 * Task here is to implement a function that says if a given string is
 * palindrome.
 * 
 * 
 * 
 * Definition=> A palindrome is a word, phrase, number, or other sequence of
 * characters which reads the same backward as forward, such as madam or
 * racecar.
 */
import javax.swing.*;
public class TASK1 {
    public static void main(String[] args) {
       
        String word="", invertedWord="";
 
        // Stores the word in a String
        word = JOptionPane.showInputDialog(null,"Digite uma palavra: "); 
        
        //Uses reverse method Stringbuffer's class
        invertedWord = new StringBuffer(word).reverse().toString();
         
        //compares word typed and word inverted
        if (word.equals(invertedWord)) 
             JOptionPane.showMessageDialog(null, word + " É uma palavra palindroma"); 
        else 
             JOptionPane.showMessageDialog(null, word + " Não é uma palavra palindroma"); 
 
        
    }
}
